import React from "react";

const Boton = (texto, funcion ) => {
    return (
        <div>
            <button id="boton" type="button" onClick={funcion()}>{texto}</button>
        </div>
    );
};

export default Boton;