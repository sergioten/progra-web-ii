// TBI
var { graphql, buildSchema } = require('graphql');
const serveStatic = require('serve-static');

const applicationId = "applicationId:SergioDiez";
const urlBase = "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development";

let putPair = (key, value) => {
    return putPair(key, value, 5);
};

let putPair2 = (key, value, cont) => {
    if (cont === 0) return false;
    fetch(
        new Request(urlBase + "pairs/" + key,
        {
            method: "PUT",
            body: value,
            headers: new Headers().append("x-application-id", applicationId)
        })
    )
    .then(response => response)
    .then(function (data){
        if (data.status <200 || data.status >= 300){
            return putPair(key, value, cont--);
        }
        return true;
    })
};

let getPair = (key) => {
    return getPair(key,5);
};

let getPair2 = (key,cont) => {
    if (cont === 0){
        return null;
    }
    fetch(
        new Request(urlBase + "pairs/" + key,{
            method: "GET",
            headers: new Headers().append("x-application-id", applicationId)
        })
    )
    .then(function (response) {return response})
    .then(function (data) {
        if (data.status <200 || data.status >= 300) {
            return getPair(key, cont--);
        } else {
            data.json().then(function (json){
                return json;
            })
        }
    })
};

let getAllPairs = (prefix) => {
    return getAllPairs2(prefix, 5);
};

let getAllPairs2 = (prefix, cont) => {
    if(cont === 0) {
        return null;
    }
    fetch(
        new Request(urlBase + "pairs/" + prefix,{
            method: "GET",
            headers: new Headers().append("x-application-id", applicationId)
        })
    )
    .then(function (response) {return response})
    .then(function (data){
        if (data.status < 200 || data.status >= 300){
            return getAllPairs2(key, cont--);
        }else{
            data.json().then(function (json){return json;})
        }
    })
};


// DATA BASE //
let nombreJugador = [
    "Jugador 1",
    "Jugador 2",
    "Jugador 3",
    "Jugador 4"
];

let nombreNivel = [
    "Nivel 1",
    "Nivel 2",
    "Nivel 3",
    "Nivel 4",
    "Nivel 5"
]

let dificultadNivel = [
    "Dificultad 1",
    "Dificultad 2",
    "Dificultad 3",
    "Dificultad 4",
    "Dificultad 5"
]

let puntosNivel = [
    "1000",
    "2000",
    "3000",
    "4000",
    "5000"
]

// SCHEMA GRAPHQL
var schema = buildSchema(`
    type Nivel {
        nombre: String!
        dificultad: String!
        puntos: Int!
    }
    
    type Muertes {
        fecha: DataTime!
    }

    type Stats {
         muertes: [Muertes!]!
         puntosMaximos: Nivel! 
    }

    type Estado {
        nivelActual: String!
        dificultadActual: String!
        puntosActuales: Int!
        muertesActuales: Muertes!
    }

    type Partida {
        id: ID!
        stats: Stats!
        estado: Estado!
        enUso: Boolean!
    }

    type Query {
        partidas: [Partida!]!
        partida(id: ID!): Partida
    }
    
    type Mutation {
        guardarPartida: Partida!
    }
`);

// RESOLVERS //
const resolversRoot = {
    Nivel: {
        nombre: (id) => {
            return nombreNivel[id];
        },
        dificultad: (nombre) => {
            return dificultadNivel[nombre];
        },
        puntos: (num) => {
            return puntos[num];

        }
    },

    Muertes: {
        totalMuertes: (numero) => {
            return (numero * numero);
        }
    },

    Stats: {
        muertes: (partidaId) => {
            //solicitamos que nos devuelva el nº de muertes
            let json = getPair(partidaId);
            if(json !== null) {
                return json.muertes;
            }else{
                return -1;
            }
        },
        puntosMaximos: (partidaId) => {
            
            let json = getPair(partidaId);
            if(json !== null) {
                return json.puntosMaximos;
            }else{
                return -1;
            }
        }
    },

    Estado: {
        nivelActual: estado.nivelActual ,
        dificultadActual: estado.dificultadActual, 
        puntosActuales: estado.puntosActuales,
        muertesActuales: estado.muertesActuales
    },

    Partida: {
        id: partida.id,
        stats: partida.stats,
        estado: partida.estado,
        enUso: partida.enUso
    },

    Query: {
        partid: id => getPair('partida-${id})')
    },

    Mutation: {
        guardarPartida: partida => {
            putPair(partida.id, partida);
        },
        cargarPartidaEnUso: partida => {
            partida.enUso = true;
            putPair(partida.id, partida);
            return partida;
        },
        cerrarPartidaEnUso: partida => {
            partida.enUso = false;
            putPair(partida.id, partida);
            return partida;
        }
    }
};

const server = new GraphQLServer({ schema, resolvers });

server.start({
    playground: "/",
    port: process.env.PORT
});
