import React, { useState } from "react";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Signup from './pages/Signup';
import Game from "./pages/Game";
import PrivateRoute from './PrivateRoute';
import { AuthContext } from "./context/auth";


export default function App() {
  const [authTokens, setAuthTokens] = useState();

  const setTokens = (data) => {
    localStorage.setItem("token", JSON.stringify(data));
    setAuthTokens(data);
  }

// NAVIGATOR | MENU

  return (
    <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
      <Router>
        <nav>
          <ul>
            <li>
              <Link activeClassName="active-menu" className="active-menu" to="/">INICIO</Link>
            </li>
            <li>
              <Link activeClassName="active-menu" to="/signup">REGISTRARSE</Link>
            </li>
            <li>
              <Link activeClassName="active-menu" to="/login">LOGIN</Link>
            </li>
            <li>
              <Link activeClassName="active-menu" to="/game">VER JUEGO</Link>
            </li>
            <li>
            </li>
          </ul>
        </nav>

          <Switch>
            <Route exact path="/"><Home /></Route>
            <Route exact path="/login"><Login /></Route>
            <Route exact path="/signup"><Signup /></Route>
            <PrivateRoute exact path="/game"><Game /></PrivateRoute>
          </Switch>
      </Router>
      
    </AuthContext.Provider>
  );
}