/*
[1] How to sign up a user with username johndoe:
  POST /users/johndoe HTTP/1.1
    [...] 
    {
      "firstName": "John",
      "password": "correcthorsebatterystaple"
    }
    201 ok
*/

import React, { Component, useState } from "react";
import Axios from 'axios';
import logoImg from "../img/logo-uneatlantico.png";
import { Redirect } from "react-router-dom";
import { Card, Logo, Form, Input, Button, } from '../components/AuthForms';
import toastr from 'toastr';
import 'toastr/build/toastr.css';

export default function Signup() {
  const [isError, setIsError] = useState(false);
  const [userName, setUserName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [password, setPassword] = useState("");
  const [userCreated, setUserCreated] = useState(false);

/* [1] Sign up a user with username johndoe*/
  function postSingup() {
    Axios.post("https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/" + userName, {
      firstName,
      password
    }).then(result => {
      if (result.status === 201) {
        setUserCreated(true);
        toastr.success(firstName + " Registrado con éxito!")
      } else {
        setIsError(true);
      }
    }).catch(e => {
      setUserCreated(false);
      toastr.error(e);
    });
  }


  if (userCreated) {
    return <Redirect to="/login" />
  }
/* FORMULARIO DE REGISTRO */
  return (
    <Card>
      <Logo src={logoImg} />
      <Form>
        <Input type="username" value={userName} onChange={e => { setUserName(e.target.value); }} placeholder="Nombre de usuario" />
        <Input type="firstName" value={firstName} onChange={e => { setFirstName(e.target.value); }} placeholder="Nombre" />
        <Input type="password" value={password} onChange={e => { setPassword(e.target.value); }} placeholder="Contraseña" />
        
        <Button onClick={postSingup}>Registrarse</Button>
      </Form>
    </Card>
  );
}
