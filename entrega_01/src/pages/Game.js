/* 
[1] How to retrieve attributes of currently logged in user. 12tndiu2t3hudicau is a valid session token obtained through log in.
  GET /user HTTP/1.1
    [...]
    Authorization: 12tndiu2t3hudicau

[2] How to invalidate session with id 12tndiu2t3hudicau.
  DELETE /sessions/12tndiu2t3hudicau HTTP/1.1
    [...]
    Authorization: 12tndiu2t3hudicau
    204 ok
*/

import Axios from "axios";
import React, { Component, useState } from "react";
import { Redirect } from "react-router-dom";
import { Button, Card, Title, CenterDiv, CardLeft } from "../components/AuthForms";
import 'toastr/build/toastr.css';
import toastr from "toastr";
import './game.css'

export default function Game() {
  const sessionToken = localStorage.getItem("token");
  const [username, setUsername] = useState("");
  const [firstName, setFirstName] = useState("");
  const [isLogOut, setIsLogOut] = useState(false);
  
/* [1]attributes of currently logged in user */  
  function getAttributes() {
    Axios.get("https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/user", {
      headers: {
        'Authorization': sessionToken
      }
    }).then(result => {
      if (result.status === 200) {
        setUsername(JSON.stringify(result.data.username));
        setFirstName(JSON.stringify(result.data.firstName));
        toastr.info("Acceso al juego concedido " + JSON.parse(JSON.stringify(result.data.firstName)));
      }
    }).catch(e => {
      toastr.error(e);
    });
  }

/* [2] invalidate session */
  function logOut() {
    //setAuthTokens();
    Axios.delete("https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/sessions/" + sessionToken, {
      headers: {
        'Authorization': sessionToken
      }
    }).then(result => {
      if (result.status === 204) {
        setIsLogOut(true);
        toastr.success("Te has deslogueado con éxito " + firstName);
        localStorage.clear();
        //setAuthTokens();
      }
    }).catch(e => {
      toastr.error(e);
      setIsLogOut(false);
    });
  }

  if (isLogOut) {
    return <Redirect to="/login" />
  }

  return (
    <div>
      <div><Title><h1>Bienvenido al TIC TAC TOE by ReactJS</h1></Title></div>
      <div>
        <CardLeft>
          <div><h4>Usuario logueado: {firstName}</h4>
            <Button onClick={getAttributes}>Get Attributes</Button>
          </div>
        </CardLeft>
      </div>
      <div>
        <CenterDiv><Card><div><GameTicTacToe /></div></Card></CenterDiv>
      </div>
      <div>
        <Card>
          <Button onClick={logOut}>Desconectarse</Button>
        </Card>
      </div>
    </div>
  );
}

// TIC TAC TOE - REACTJS TUTORIAL //
/* https://codepen.io/gaearon/pen/gWWZgR */

function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
      />
    );
  }

  render() {
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

class GameTicTacToe extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [
        {
          squares: Array(9).fill(null)
        }
      ],
      stepNumber: 0,
      xIsNext: true
    };
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? "X" : "O";
    this.setState({
      history: history.concat([
        {
          squares: squares
        }
      ]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0
    });
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares);

    const moves = history.map((step, move) => {
      const desc = move ?
        'Ir al movimiento #' + move :
        'Empezar de nuevo';
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    let status;
    if (winner) {
      status = "Ganador: " + winner;
    } else {
      status = "Siguiente Jugador: " + (this.state.xIsNext ? "X" : "O");
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={i => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

// == GANADOR ==


function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

